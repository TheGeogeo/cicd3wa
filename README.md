# Documentation
Lien vers la documentation : https://docs.google.com/document/d/1-_aWOwU2rpciMVV8Qdw0ujNh0isPaQW5oM0HbllwYec/edit?usp=sharing

# Installation

- Cloner le projet : https://gitlab.com/TheGeogeo/cicd3wa.git
- Ouvrir un terminal de commande (debian, ou terminal vscode)
- Se déplacer à la racine du projet “cd cicd3wa”
- Lancer la commande docker compose up --build -d
- Se connecter au container “backend” en récupérant l’id du dit container en lançant “docker ps”
- Puis faire “docker exec -it “container_id” /bin/bash”
- Lancer “composer install”
- Lancer php bin/console d:m:m
- l’application est prête à être utilisée !

# Fixtures
- Se connecter au container “backend” en récupérant l’id du dit container en lançant “docker ps”
- Puis faire “docker exec -it “container_id” /bin/bash”
- Lancer php bin/console doctrine:fixtures:load

# PhpMyAdmin 

- Se rendre sur localhost:8336
- Se connecter avec : serveur = database; user = user; password = password
