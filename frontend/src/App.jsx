import './App.css';
import React, {useEffect, useState} from "react";
import AppContext from "./context/AppContext";
import {fetchJson} from "./utils/fetch";
import Snackbar from "@mui/material/Snackbar";
import Alert from "./components/Alert/Alert";
import NavBar from './components/NavBar/NavBar';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from './components/Login/Login';
import Notifications from '@mui/icons-material/Notifications';
import SignIn from "./components/SignIn/SignIn";
import Profile from "./components/Profile/Profile";
import Home from "./components/Home/Home";

const App = () => {
  // On importe les variables d'environnement
  const { REACT_APP_USER } = process.env;

  const emptyUser= {
    id: 5,
    firstName: "John",
    lastName: "Doe",
    email: "toto@toto.com",
  }

  // Tous les state de App.jsx
  const [sessionToken, setSessionToken] = useState(null);
  const [user, setUser] = useState(emptyUser);
  const [posts, setPosts] = useState([]);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [openError, setOpenError] = useState(false);

  // Fonction pour récupérer l'utilisateur
  const fetchUser = async() => {
    try{
      return await fetchJson(REACT_APP_USER);
    } catch(e){
      console.error(e);
    }
  }

  // Quand le token est présent, on récupère l'utilisateur
  useEffect(() => {
    if(sessionToken) {
      setSessionToken(sessionToken);
      fetchUser().then(user => {
        setUser(user);
      })
    }
  }, [sessionToken])

  // On met à jour le state du token
  useEffect(() => {
    // setSessionToken(localStorage.getItem('token'));
  }, []);

  // Fonction de fermeture du snackbar de succès
  const handleCloseSuccess = (event, reason) => {
    if (reason === 'clickaway')
      return;
    setOpenSuccess(false);
  };

  // Fonction de fermeture du snackbar d'erreur
  const handleCloseError = (event, reason) => {
    if (reason === 'clickaway')
      return;
    setOpenError(false);
  };

  // Données de context
  let providerData = {
    sessionToken,
    setSessionToken,
    user,
    setUser,
    fetchUser,
    posts,
    setPosts,
    openSuccess,
    setOpenSuccess,
    openError,
    setOpenError,
    handleCloseSuccess,
    handleCloseError,
  };

  return (
    <AppContext.Provider value={providerData}>
      <div className="App">
        <Router>
          <NavBar />
          <Routes>
            <Route path="/" element={<Home />}/>
            <Route path="/notifications" element={<Notifications />}/>
            <Route path="/profile" element={<Profile />}/>
            <Route path="/sign_in" element={<SignIn />}/>
            <Route path="/Login" element={<Login />} />
          </Routes>
        </Router>
      </div>
      <Snackbar open={openSuccess} autoHideDuration={6000} onClose={handleCloseSuccess}>
        <Alert onClose={handleCloseSuccess} severity="success" sx={{ width: '100%' }}>
          You've been logged in!
        </Alert>
      </Snackbar>
    </AppContext.Provider>
  );
}

export default App;