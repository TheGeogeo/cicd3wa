import "./home.css"
import React, {useContext, useState} from "react";
import PostCard from "../Post/Post";
import Button from '@mui/material/Button';
import PostModal from "../PostModal/PostModal";
import AppContext from "../../context/AppContext";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { fetchPost } from "../../utils/fetch";

const Home = () => {

  const navigate = useNavigate();
  const {sessionToken, posts, setPosts, user} = useContext(AppContext);
  const { REACT_APP_POST } = process.env;

  useEffect(() => {
    if (!sessionToken) {
      navigate('/login')
    } else {
      // posts exemple
      setPosts([
        {
          id: 0,
          user: {
            id: 5,
            firstName: "John",
            lastName: "Doe"
          },
          content: "Why did John Doe go to therapy? Because he needed help finding his identity - turns out he was feeling a bit anonymous!",
          img: "",
          date: new Date(),
          comments: [
            {
              user: {
                id: 4,
                firstName: "John",
                lastName: "Doe"
              },
              content: "that's a good joke haha !",
              date: new Date(),
            },
            {
              user: {
                id: 3,
                firstName: "John",
                lastName: "Doe"
              },
              content: "A very good one indeed",
              date: new Date(),
            }
          ]
        },
        {
          id: 1,
          user: {
            id: 1,
            firstName: "John",
            lastName: "Doe"
          },
          content: "Why did John Doe go to therapy? Because he needed help finding his identity - turns out he was feeling a bit anonymous!",
          img: "",
          date: new Date(),
          likes: [],
          comments: [
            {
              user: {
                firstName: "John",
                lastName: "Doe"
              },
              content: "that's a good joke haha !",
              date: new Date(),
            },
            {
              user: {
                id: 2,
                firstName: "John",
                lastName: "Doe"
              },
              content: "A very good one indeed",
              date: new Date(),
            }
          ]
        },
        {
          id: 2,
          user: {
            id: 5,
            firstName: "John",
            lastName: "Doe"
          },
          content: "Why did John Doe go to therapy? Because he needed help finding his identity - turns out he was feeling a bit anonymous!",
          img: "",
          date: new Date(),
          likes: [],
          comments: [
            {
              user: {
                firstName: "John",
                lastName: "Doe"
              },
              content: "that's a good joke haha !",
              date: new Date(),
            },
            {
              user: {
                id: 2,
                firstName: "John",
                lastName: "Doe"
              },
              content: "A very good one indeed",
              date: new Date(),
            }
          ]
        },
        {
          id: 3,
          user: {
            id: 6,
            firstName: "John",
            lastName: "Doe"
          },
          content: "Why did John Doe go to therapy? Because he needed help finding his identity - turns out he was feeling a bit anonymous!",
          img: "",
          date: new Date(),
          likes: [],
          comments: [
            {
              user: {
                firstName: "John",
                lastName: "Doe"
              },
              content: "that's a good joke haha !",
              date: new Date(),
            },
            {
              user: {
                id: 2,
                firstName: "John",
                lastName: "Doe"
              },
              content: "A very good one indeed",
              date: new Date(),
            }
          ]
        }
      ]);
    }
  }, [])


  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleCreateComment = async (data) => {
    console.log(data);
    const newComment = {
      post_id: posts[data.index].id,
      user_id: user.id,
      content: data.content,
      date: new Date(),
    }

    posts[data.index].comments.push(newComment)

    const addCommentToPost = await fetchPost(`${REACT_APP_POST}/${posts[data.index].id}`, newComment)
    addCommentToPost.then((res) => {
      setPosts([
        ...posts,
        res
      ]);
    })

    setPosts([...posts])
  };

  return (
    <div className="mainFeed">
      <div className="homeDiv">
        <div className="newsFeed">
          { posts && posts.length > 0 && posts.map((post, index) => {
              return <PostCard key={index} postContent={post} onSubmit={handleCreateComment} idx={index}/>
            })
          }
        </div>
        <div>
          <Button id="newPostButton" variant="contained" onClick={handleOpenModal}>
            Create New Post
          </Button>
          <PostModal
            isOpen={isModalOpen}
            onClose={handleCloseModal}
          />
        </div>
      </div>
      <div className="leftSideDiv"></div>
      <div className="rightSideDiv"></div>
    </div>
  )
}

export default Home;