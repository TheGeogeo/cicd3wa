import "./navbar.css"
import React, { useContext, useEffect, useState } from 'react';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MailIcon from '@mui/icons-material/Mail';
import NotificationsIcon from '@mui/icons-material/Notifications';
import AppContext from "../../context/AppContext";
import { Link, useNavigate } from "react-router-dom";

const NavBar = () => {
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [profileMenu, setProfileMenu] = useState(null);
    const navigate = useNavigate();
    
    // Auth token checked on component mount
    // Default: false 
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const { sessionToken, setUser } = useContext(AppContext);
    
    // on mount
    useEffect(() => {
        // check auth token to log back user
        if(sessionToken)
            setIsLoggedIn(true);
    }, [sessionToken, setIsLoggedIn])

    const handleProfileMenuOpen = (event) => {
        setProfileMenu(event.currentTarget);
    }
    
    const handleMenuClose = (event) => {
        handleProfileMenuOpen(event)
        setIsMenuOpen(isMenuOpen ? false : true);
    }

    const handleLogOut = () => {
        localStorage.removeItem("token");
        setUser({
            id: "",
            firstName: "",
            lastName: "",
            email: "",
        });
        window.reload();
    }

    return (
        <header className="App-header">
            <nav className="navbar">
                <div className="logo"><Link to="/">Blogyblog</Link></div>
                <ul className="nav-links">
                    <Link to="/messages">
                        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
                            <Badge badgeContent={4} color="error">
                                <MailIcon />
                            </Badge>
                        </IconButton>
                    </Link>
                    <IconButton
                    size="large"
                    aria-label="show 17 new notifications"
                    color="inherit"
                    >
                    <Badge badgeContent={17} color="error">
                        <NotificationsIcon />
                    </Badge>
                    </IconButton>
                    <IconButton
                        size="large"
                        edge="end"
                        aria-label="account of current user"
                        aria-haspopup="true"
                        onClick={handleMenuClose}
                        color="inherit"
                        >
                        <AccountCircle />
                    </IconButton>
                    <Menu
                        anchorEl={profileMenu}
                        anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                        }}
                        open={isMenuOpen}
                        onClose={handleMenuClose}
                    >
                        { isLoggedIn &&
                            <ul>
                                <MenuItem onClick={handleMenuClose}><Link to="/profile">Profile</Link></MenuItem>
                                <MenuItem onClick={handleMenuClose}><Link to="/settings">My account</Link></MenuItem>
                                <MenuItem onClick={handleLogOut}><Link to="/">Log out</Link></MenuItem>
                            </ul>
                        }
                        { !isLoggedIn &&
                            <ul>
                                <MenuItem onClick={handleMenuClose}><Link to="/sign_in">Sign in</Link></MenuItem>
                                <MenuItem onClick={handleMenuClose}><Link to="/Login">Log in</Link></MenuItem>
                            </ul>
                        }
                    </Menu>                            
                </ul>
            </nav>
        </header>
    )    
}

export default NavBar;