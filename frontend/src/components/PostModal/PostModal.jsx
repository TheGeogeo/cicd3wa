import React, { useContext, useState } from 'react';
import { Button, Modal, Fade, TextField, Typography, useTheme} from '@mui/material';
import { styled } from '@mui/system';
import "./postModal.css"
import AppContext from '../../context/AppContext';
import { fetchPost } from '../../utils/fetch';

const StyledModal = styled(Modal)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledPaper = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
  boxShadow: theme.shadows[5],
  padding: theme.spacing(2, 4, 3),
}));


const PostModal = ({ isOpen, onClose, onSubmit }) => {
  const theme = useTheme();
  const [content, setContent] = useState('');
  const [title, setTitle] = useState('');
  const {REACT_APP_POST} = process.env

  const handleContentChange = (e) => setContent(e.target.value);
  const handleTitleChange = (e) => setTitle(e.target.value);
  const { user, posts, setPosts, sessionToken } = useContext(AppContext);

  const handleCreatePost = async () => {
    const newPost = {
      jwt: sessionToken,
      title: title,
      content: content,
    };

    const addPost = await fetchPost(`${REACT_APP_POST}`, newPost)
    addPost.then((res) => {
      setPosts([...res["hydra:member"]]);
    })
    setContent('');
    onClose();
  };

  return (
    <StyledModal
      open={isOpen}
      onClose={onClose}
      closeAfterTransition
    >
      <Fade in={isOpen}>
        <StyledPaper theme={theme}>
          <Typography variant="h6">New Post</Typography>
          <TextField
            label="title"
            variant="outlined"
            fullWidth
            margin="normal"
            multiline
            value={title}
            onChange={handleTitleChange}
          />
          <TextField
            label="content"
            variant="outlined"
            fullWidth
            margin="normal"
            multiline
            rows={4}
            value={content}
            onChange={handleContentChange}
          />
          <Button variant="contained" color="primary" onClick={handleCreatePost}>
            Create Post
          </Button>
        </StyledPaper>
      </Fade>
    </StyledModal>
  );
};

export default PostModal;