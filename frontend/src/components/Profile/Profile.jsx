import React, { useContext, useState } from 'react';
import {
  TextField,
  Button,
  Typography,
  Container,
} from '@mui/material';
import AppContext from '../../context/AppContext';
import { useEffect } from 'react';
import { fetchPatch } from '../../utils/fetch';

const Profile = () => {

  const { user, setUser } = useContext(AppContext);
  const { REACT_API_USER } = process.env;

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: '',
    email: '',
    bio: '',
  });

  useEffect(() => {
    setFormData({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.name,
      bio: user.bio
    })
  })

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log('Form submitted:', formData);
    const updateProfile = await fetchPatch(REACT_API_USER, formData)
    updateProfile.then((res) => {
      setUser(res);
    })
  };

  return (
    <Container className="formContainer">
      <Typography variant="h5" align="center">
        Profile Page
      </Typography>
      <form className="form" onSubmit={handleSubmit}>
        <TextField
          className="formField"
          label="firstName"
          name="firstName"
          variant="outlined"
          value={user.firstName}
          onChange={handleInputChange}
          fullWidth
        />
        <TextField
          className="formField"
          label="lastName"
          name="lastName"
          variant="outlined"
          value={user.lastName}
          onChange={handleInputChange}
          fullWidth
        />
        <TextField
          className="formField"
          label="Email"
          name="email"
          type="email"
          variant="outlined"
          value={user.email}
          onChange={handleInputChange}
          fullWidth
        />
        <TextField
          className="formField"
          label="Bio"
          name="bio"
          multiline
          rows={4}
          variant="outlined"
          value={user.bio}
          onChange={handleInputChange}
          fullWidth
        />
        <Button
          className="formField"
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
        >
          Update Profile
        </Button>
      </form>
    </Container>
  );
};

export default Profile;
