import React from "react";
import MuiAlert from "@mui/material/Alert";

// Composant d'alerte reposant sur le composant MuiAlert et l'attribut ref de React
const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default Alert;