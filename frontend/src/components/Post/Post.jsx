import React, {useState} from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { TextField} from '@mui/material';
import "./post.css"
import { useContext } from 'react';
import AppContext from "../../context/AppContext";
import { useEffect } from 'react';
import SendIcon from '@mui/icons-material/Send';
import { fetchDelete, fetchJson, fetchPatch, fetchPost } from '../../utils/fetch';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const PostCard = ({postContent, onSubmit, idx}) => {
  const [expanded, setExpanded] = useState(false);
  const [content, setContent] = useState('');
  const { user, posts, setPosts} = useContext(AppContext);
  const { REACT_APP_POST, REACT_APP_LIKE } = process.env;

  const handleContentChange = (e) => setContent(e.target.value)
  
  // on mount
  useEffect(() => {

  }, [])

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleSubmit = (e) => {
    onSubmit({ index: idx, content });
    setContent('');
  };

  const handleLikeButton = async (e) => {

    const postIndex = posts.findIndex(post => post.id === e.target.id);

    if (postIndex !== -1) {
      const likeIndex = posts[postIndex].likes.findIndex(like => like.user_id === user.id);
      if (likeIndex !== -1) {
        const like = posts[postIndex].likes.filter(like => like.user_id === user.id)
        const deleteLike = await fetchDelete(REACT_APP_LIKE, like.id)
        deleteLike.then(() => {
          posts[postIndex].likes = posts[postIndex].likes.filter(like => like.user_id !== user.id)
          setPosts([...posts])
        })
      } else {
        const addLike = await fetchPost(REACT_APP_LIKE, user.id)
        addLike.then(async () => {
          const updatedPosts = await fetchJson(REACT_APP_POST)
          updatedPosts.then((res) => {
            setPosts([...res]);
          })
        });
      }
    }
    const postLikesUpdate = await fetchPatch(REACT_APP_POST, posts[postIndex].id, posts[postIndex])
    postLikesUpdate.then((res) => {
      setPosts([...res]);
    })
  }

  const handleDeleteClick = async (postId) => {
    const deletePost = await fetchDelete(REACT_APP_POST, postId);
    deletePost.then((res) => {
      setPosts([...res])
    })
  };

  return (
    <Card className='homeCard'>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="post">
            {postContent.user.firstName[0]}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={postContent.user.firstName + " " + postContent.user.lastName}
        subheader={postContent.date.toLocaleDateString()}
      />
      <CardContent sx={{padding:"0 16px"}}>
        <Typography variant="body2" color="text.secondary">
          {postContent.content}
        </Typography>
        { false &&
          <CardMedia
            component="img"
            height="194"
            image=""
            alt=""
          />
        }
        <div className="commentInput">
          <TextField
            label="Comment"
            variant="filled"
            fullWidth
            margin="normal"
            multiline
            value={content}
            onChange={handleContentChange}
            sx={{marginTop: 0}}
          />
          <IconButton aria-label="share" onClick={(e) => {handleSubmit(e)}}>
            <SendIcon />
          </IconButton>
        </div>
        <CardActions disableSpacing sx={{display:"flex", justifyContent: "flex-end"}}>
        { user && user.id !== postContent.user.id &&
          <IconButton id={postContent.id} aria-label="add to favorites" onClick={(e) => {handleLikeButton(e)}}>
            <FavoriteIcon />
          </IconButton>
        }
          <IconButton aria-label="share">
            <ShareIcon />
          </IconButton>
          <button onClick={() => handleDeleteClick(postContent.id)}>Delete Post</button>
          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </ExpandMore>
        </CardActions>
      </CardContent>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          { postContent.comments.map((com, index) => {
              return (
                <Card sx={{marginBottom: "10px"}}>
                  <CardHeader
                    avatar={
                      <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                        {postContent.user.firstName[0]}
                      </Avatar>
                    }
                    title={postContent.user.firstName + " " + postContent.user.lastName}
                    subheader={postContent.date.toLocaleDateString()}
                  />
                  <Typography key={index} paragraph sx={{paddingLeft: "16px"}}>
                    { com.content }
                  </Typography>
                </Card>
              );
            })
          }
        </CardContent>
      </Collapse>
    </Card>
  );
}

export default PostCard;