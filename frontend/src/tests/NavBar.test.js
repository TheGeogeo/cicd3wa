// NavBar.test.js
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import NavBar from '../components/NavBar/NavBar';
import AppContext from '../context/AppContext';
import { useLocation } from 'react-router-dom';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: jest.fn(),  // Mock the entire hook
}));

describe('NavBar component', () => {
  beforeEach(() => {
    // Mock the useState hook
    jest.spyOn(React, 'useState').mockImplementation((initialValue) => [initialValue, jest.fn()]);
  });

  afterEach(() => {
    // Restore the original implementation after each test
    jest.restoreAllMocks();
  });

  it('renders without crashing', () => {
    // Mock the context values
    const mockContextValue = { _context: { sessionToken: true } };
    jest.spyOn(React, 'useContext').mockReturnValue(mockContextValue);

    // Mock the useLocation hook to provide a pathname
    useLocation.mockReturnValue({ pathname: '/' });

    render(<NavBar />);

    expect(screen.getByText('Blogyblog')).toBeInTheDocument();
    expect(screen.getByText('Sign in')).toBeInTheDocument();
    expect(screen.getByText('Log in')).toBeInTheDocument();
    expect(screen.queryByText('Profile')).not.toBeInTheDocument();
    expect(screen.queryByText('My account')).not.toBeInTheDocument();
  });

  it('opens menu when button is clicked', () => {
    const mockContextValue = { _context: { sessionToken: true } };
    jest.spyOn(React, 'useContext').mockReturnValue(mockContextValue);

    // Mock the useLocation hook to provide a pathname
    useLocation.mockReturnValue({ pathname: '/' });

    render(<NavBar />);
    const accountButton = screen.getByLabelText('account of current user');

    fireEvent.click(accountButton);
  });
});
