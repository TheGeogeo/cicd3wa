<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordHasherInterface $hasher,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('pierre@test.fr');
        $user->setFirstName('Pierre');
        $user->setLastName('Farge');
        $user->setRoles(['ROLE_USER']);
        $user->setCreatedAt(new \DateTimeImmutable);
        $user->setUpdatedAt(new \DateTimeImmutable);

        $password = $this->hasher->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        $admin = new User();
        $admin->setEmail('admin@test.fr');
        $admin->setFirstName('admin');
        $admin->setLastName('admin');
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setCreatedAt(new \DateTimeImmutable);
        $admin->setUpdatedAt(new \DateTimeImmutable);

        $password = $this->hasher->hashPassword($admin, 'password');
        $admin->setPassword($password);

        $manager->persist($admin);

        //-----------------------------------

        for ($i = 0; $i < 3; $i++) {
            $post = new Post();
            $post->setTitle("Premier titre de poste $i");
            $post->setContent("Le contenue de poste est si vide qu'il devrait être vénéré. $i");
            $post->setCreator($user);
            $post->setCreatedAt(new \DateTimeImmutable);
            $post->setUpdatedAt(new \DateTimeImmutable);

            $manager->persist($post);
        }

        //-----------------------------------

        for ($i = 0; $i < 3; $i++) {
            $comment = new Comment();
            $comment->setCreator($user);
            $comment->setContent("ceci est un commentaire" . $i);
            $comment->setCreatedAt(new \DateTimeImmutable);
            $comment->setUpdatedAt(new \DateTimeImmutable);
            $comment->setPost($post);

            $manager->persist($comment);
        }

        //-----------------------------------

        for ($i = 0; $i < 3; $i++) {
            $like = new Like();
            $like->setWho($user);
            $like->setPost($post);
            $like->setCreatedAt(new \DateTimeImmutable);

            $manager->persist($like);
        }

        //-----------------------------------
        $manager->flush();
    }
}
