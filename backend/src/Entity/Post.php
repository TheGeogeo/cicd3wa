<?php

namespace App\Entity;

use App\Entity\User;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use App\Traits\TimestampAtTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PostRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post as PostApi;
use App\State\PostProcessor;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ApiResource]
#[Get]
#[GetCollection]
#[Put(
    security: "is_granted('ROLE_ADMIN') or object.getCreator() == user",
    normalizationContext: ["groups" => ["post:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["post:write"]],
    processor: PostProcessor::class,
)]
#[Patch(
    security: "is_granted('ROLE_ADMIN') or object.getCreator() == user",
    normalizationContext: ["groups" => ["post:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["post:write"]],
    processor: PostProcessor::class,
)]
#[Delete(security: "is_granted('ROLE_ADMIN') or object.getCreator() == user")]
#[PostApi(
    security: "is_granted('ROLE_ADMIN') or is_granted('ROLE_USER')",
    normalizationContext: ["groups" => ["post:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["post:write"]],
    processor: PostProcessor::class,
)]
class Post
{
    use TimestampAtTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["post:read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["post:read", "post:write"])]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["post:read", "post:write"])]
    private ?string $content = null;

    #[ORM\OneToMany(mappedBy: 'post', targetEntity: Like::class)]
    #[Groups(["post:read"])]
    private Collection $likes;

    #[ORM\ManyToOne(inversedBy: 'posts')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["post:read"])]
    private ?User $creator = null;

    #[ORM\OneToMany(mappedBy: 'post', targetEntity: Comment::class)]
    private Collection $comments;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection<int, Like>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): static
    {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setPost($this);
        }

        return $this;
    }

    public function removeLike(Like $like): static
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getPost() === $this) {
                $like->setPost(null);
            }
        }

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): static
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): static
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): static
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getPost() === $this) {
                $comment->setPost(null);
            }
        }

        return $this;
    }
}
