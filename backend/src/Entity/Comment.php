<?php

namespace App\Entity;

use App\Entity\User;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use App\Traits\TimestampAtTrait;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use App\State\CommentProcessor;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\CommentRepository;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post as PostApi;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
#[ApiResource]
#[Get]
#[GetCollection]
#[Put(
    security: "is_granted('ROLE_ADMIN') or object.getCreator() == user",
    normalizationContext: ["groups" => ["comment:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["comment:write"]],
    processor: CommentProcessor::class,
)]
#[Patch(
    security: "is_granted('ROLE_ADMIN') or object.getCreator() == user",
    normalizationContext: ["groups" => ["comment:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["comment:write"]],
    processor: CommentProcessor::class,
)]
#[Delete(security: "is_granted('ROLE_ADMIN') or object.getCreator() == user")]
#[PostApi(
    security: "is_granted('ROLE_ADMIN') or is_granted('ROLE_USER')",
    normalizationContext: ["groups" => ["comment:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["comment:write"]],
    processor: CommentProcessor::class,
)]
class Comment
{
    use TimestampAtTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["comment:read"])]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(["comment:read", "comment:write"])]
    private ?string $content = null;

    #[ORM\ManyToOne(inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["comment:read"])]
    private ?User $creator = null;

    #[ORM\ManyToOne(inversedBy: 'comments')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["comment:read", "comment:write"])]
    private ?Post $post = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): static
    {
        $this->creator = $creator;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): static
    {
        $this->post = $post;

        return $this;
    }
}
