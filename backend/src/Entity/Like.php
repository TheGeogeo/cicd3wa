<?php

namespace App\Entity;

use App\Entity\User;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\LikeRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post as PostApi;
use App\State\LikeProcessor;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Validator as AcmeAssert;

#[ORM\Entity(repositoryClass: LikeRepository::class)]
#[ORM\Table(name: '`like`')]
#[AcmeAssert\UniqueLikeConstraint]
#[ApiResource]
#[Get]
#[GetCollection]
#[Put(
    security: "is_granted('ROLE_ADMIN') or object.getWho() == user",
    normalizationContext: ["groups" => ["like:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["like:write"]],
    processor: LikeProcessor::class,
)]
#[Patch(
    security: "is_granted('ROLE_ADMIN') or object.getWho() == user",
    normalizationContext: ["groups" => ["like:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["like:write"]],
    processor: LikeProcessor::class,
)]
#[Delete(security: "is_granted('ROLE_ADMIN') or object.getWho() == user")]
#[PostApi(
    security: "is_granted('ROLE_ADMIN') or is_granted('ROLE_USER')",
    normalizationContext: ["groups" => ["like:read", "timestamptrait:read"]],
    denormalizationContext: ["groups" => ["like:write"]],
    processor: LikeProcessor::class,
)]
class Like
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["like:read"])]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'likes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["like:read", "like:write"])]
    private ?Post $post = null;

    #[ORM\Column]
    #[Groups(["like:read"])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\ManyToOne(inversedBy: 'likes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["like:read"])]
    private ?User $who = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): static
    {
        $this->post = $post;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getWho(): ?User
    {
        return $this->who;
    }

    public function setWho(?User $who): static
    {
        $this->who = $who;

        return $this;
    }
}
