<?php

namespace App\Validator;

use App\Entity\Like;
use App\Repository\LikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueLikeConstraintValidator extends ConstraintValidator
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function validate($entity, Constraint $constraint)
    {
        /** @var Like $entity */
        if (null === $entity->getPost() || null === $entity->getWho()) {
            return;
        }

        /** @var LikeRepository $likeRepository */
        $likeRepository = $this->entityManager->getRepository(Like::class);
        $existingEntity = $likeRepository->findOneBy([
            'post' => $entity->getPost(),
            'who' => $entity->getWho(),
        ]);

        if ($existingEntity) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
