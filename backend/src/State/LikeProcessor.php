<?php

namespace App\State;

use App\Entity\Like;
use App\Repository\LikeRepository;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class LikeProcessor implements ProcessorInterface
{
    public function __construct(
        private ProcessorInterface $persistProcessor,
        private ProcessorInterface $removeProcessor,
        private UserPasswordHasherInterface $userPasswordHasherInterface,
        private Security $security,
        private LikeRepository $likeRepository,
    ) {
    }

    /**
     * @return T
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data instanceof Like) {
            /** @var Like $data */
            if ($data->getId() == null) {
                $data->setCreatedAt(new \DateTimeImmutable);
                $data->setWho($this->security->getUser());
            }
        }

        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
