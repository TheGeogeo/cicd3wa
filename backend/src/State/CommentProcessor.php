<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use App\Repository\CommentRepository;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Comment;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CommentProcessor implements ProcessorInterface
{
    public function __construct(
        private ProcessorInterface $persistProcessor,
        private ProcessorInterface $removeProcessor,
        private UserPasswordHasherInterface $userPasswordHasherInterface,
        private Security $security,
        private CommentRepository $commentRepository,
    ) {
    }

    /**
     * @return T
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data instanceof Comment) {
            /** @var Comment $data */
            if ($data->getId() == null) {
                $data->setCreatedAt(new \DateTimeImmutable);
                $data->setCreator($this->security->getUser());
            }

            $data->setUpdatedAt(new \DateTimeImmutable);
        }

        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
