<?php

namespace App\State;

use App\Entity\Post;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Repository\PostRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PostProcessor implements ProcessorInterface
{
    public function __construct(
        private ProcessorInterface $persistProcessor,
        private ProcessorInterface $removeProcessor,
        private UserPasswordHasherInterface $userPasswordHasherInterface,
        private Security $security,
        private PostRepository $postRepository,
    ) {
    }

    /**
     * @return T
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data instanceof Post) {
            /** @var Post $data */
            if ($data->getId() == null) {
                $data->setCreatedAt(new \DateTimeImmutable);
                $data->setCreator($this->security->getUser());
            }
           
            $data->setUpdatedAt(new \DateTimeImmutable);
        }

        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
