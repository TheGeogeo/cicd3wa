<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserProcessor implements ProcessorInterface
{
    public function __construct(
        private ProcessorInterface $persistProcessor,
        private ProcessorInterface $removeProcessor,
        private UserPasswordHasherInterface $userPasswordHasherInterface,
        private Security $security,
        private UserRepository $userRepository,
    ) {
    }

    /**
     * @return T
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        if ($data instanceof User) {
            /** @var User $data */
            if ($data->getId() == null) {
                $data->setCreatedAt(new \DateTimeImmutable);
                $data->setRoles(['ROLE_USER']);
            }

            $data->setUpdatedAt(new \DateTimeImmutable);
        }

        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
