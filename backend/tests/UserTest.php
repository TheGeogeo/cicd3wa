<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserTest extends WebTestCase
{
    private \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetCollection(): void
    {
        $this->client->request('GET', '/api/users');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(2,$content["hydra:totalItems"]);
    }

    public function testGetValidId(): void
    {
        $this->client->request('GET', '/api/users/1');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(1,$content["id"]);
    }

    public function testGetInvalidId(): void
    {
        $this->client->request('GET', '/api/users/99');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testPost(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "email" => "test@test.fr",
            "password" => "testtest",
            "firstName" => "test",
            "lastName" => "test"
        ];
        $this->client->request('POST', '/api/users', [], [], ['CONTENT_TYPE' => 'application/ld+json'], json_encode($jsonData));

        $this->assertInstanceOf(Response::class, $this->client->getResponse());

        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneBy(["email" => "test@test.fr"]);
        $this->assertNotNull($user->getId());
    }

    public function testPatch(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "email" => "test@test.fr",
            "password" => "testtest",
            "firstName" => "test modifié",
            "lastName" => "test"
        ];

        $this->client->request('PATCH', '/api/users/1', [], [], ['CONTENT_TYPE' => 'application/merge-patch+json'], json_encode($jsonData));
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneBy(["id" => 1]);

        $this->assertEquals("test modifié", $user->getFirstName());

    }
}