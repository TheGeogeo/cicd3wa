<?php

namespace App\Tests;

use App\Repository\CommentRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CommentTest extends WebTestCase
{
    private \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetCollection(): void
    {
        $this->client->request('GET', '/api/comments?page=1');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(3,$content["hydra:totalItems"]);
    }

    public function testGetValidId(): void
    {
        $this->client->request('GET', '/api/comments/1');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(1,$content["id"]);
    }
    public function testGetInvalidId(): void
    {
        $this->client->request('GET', '/api/comments/99');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testPost(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "content" => "ceci est un commentaire test",
            "post"=> "/api/posts/1"
        ];
        $this->client->request('POST', '/api/comments', [], [], ['CONTENT_TYPE' => 'application/ld+json'], json_encode($jsonData));

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertInstanceOf(Response::class, $this->client->getResponse());

        $commentRepository = static::getContainer()->get(CommentRepository::class);
        $comment = $commentRepository->findOneBy(["content" => "ceci est un commentaire test"]);
        $this->assertNotNull($comment->getId());

    }

    public function testPatch(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "content"=> "ceci est un commentaire modifié",
            "post" => "/api/posts/1"
        ];

        $this->client->request('PATCH', '/api/comments/1', [], [], ['CONTENT_TYPE' => 'application/merge-patch+json'], json_encode($jsonData));


        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $commentRepository = static::getContainer()->get(CommentRepository::class);
        $comment = $commentRepository->findOneBy(["id" => 1]);

        $this->assertEquals("ceci est un commentaire modifié", $comment->getContent());
    }

    public function testDelete(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $this->client->request('DELETE', '/api/comments/1');
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }
}