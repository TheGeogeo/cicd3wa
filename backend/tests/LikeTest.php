<?php

namespace App\Tests;

use App\Repository\LikeRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class LikeTest extends WebTestCase
{
    private \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetCollection(): void
    {
        $this->client->request('GET', '/api/likes');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(3,$content["hydra:totalItems"]);
    }

    public function testGetValidId(): void
    {
        $this->client->request('GET', '/api/likes/1');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(1,$content["id"]);
    }
    public function testGetInvalidId(): void
    {
        $this->client->request('GET', '/api/likes/99');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testPost(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "post" => "api/posts/1"
        ];
        $this->client->request('POST', '/api/likes', [], [], ['CONTENT_TYPE' => 'application/ld+json'], json_encode($jsonData));
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $likeRepository = static::getContainer()->get(LikeRepository::class);
        $like = $likeRepository->findOneBy(["post" => "1"]);
        $this->assertNotNull($like->getId());
    }

    public function testPatch(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "post" => "api/posts/2"
        ];

        $this->client->request('PATCH', '/api/likes/1', [], [], ['CONTENT_TYPE' => 'application/merge-patch+json'], json_encode($jsonData));
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $likeRepository = static::getContainer()->get(LikeRepository::class);
        $like = $likeRepository->findOneBy(["id" => 1]);

        $this->assertEquals(2, $like->getPost()->getId());

    }

    public function testDelete(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $this->client->request('DELETE', '/api/likes/1');
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());
    }
}