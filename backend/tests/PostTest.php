<?php

namespace App\Tests;

use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PostTest extends WebTestCase
{
    private \Symfony\Bundle\FrameworkBundle\KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetCollection(): void
    {
        $this->client->request('GET', '/api/posts');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(3,$content["hydra:totalItems"]);
    }

    public function testGetValidId(): void
    {
        $this->client->request('GET', '/api/posts/1');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $content = json_decode($this->client->getResponse()->getContent(),true);
        $this->assertEquals(1,$content["id"]);
    }
    public function testGetInvalidId(): void
    {
        $this->client->request('GET', '/api/posts/99');

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testPost(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "title" => "titre test",
            "content" => "content"
        ];
        $this->client->request('POST', '/api/posts', [], [], ['CONTENT_TYPE' => 'application/ld+json'], json_encode($jsonData));

        $this->assertInstanceOf(Response::class, $this->client->getResponse());
        $postRepository = static::getContainer()->get(PostRepository::class);
        $post = $postRepository->findOneBy(["title" => "titre test"]);
        $this->assertNotNull($post->getId());
    }

    public function testPatch(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $jsonData = [
            "title" => "titre modifié",
            "content" => "content"
        ];

        $this->client->request('PATCH', '/api/posts/1', [], [], ['CONTENT_TYPE' => 'application/merge-patch+json'], json_encode($jsonData));


        $postRepository = static::getContainer()->get(PostRepository::class);
        $post = $postRepository->findOneBy(["id" => 1]);

        $this->assertEquals("titre modifié", $post->getTitle());

    }

    public function testDelete(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail('pierre@test.fr');
        $this->client->loginUser($user);

        $this->client->request('DELETE', '/api/posts/1');
        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

    }
}